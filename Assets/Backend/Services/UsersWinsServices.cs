﻿using Assets.Backend.Classes;
using Assets.Backend.Enums;
using System;
using System.Collections.Generic;

namespace Assets.Backend.Services
{
    /// <summary>
    /// Сервис, для занесение победителя
    /// </summary>
    public class UsersWinsServices
    {
        private readonly FantomHH fantom;

        public UsersWinsServices() { fantom = FantomHH.GetInstance(); }

        /// <summary>
        /// Создание записи
        /// </summary>
        /// <param name="mobileguid">Идентификатор устройства</param>
        /// <param name="messageAction">При успехе в сообщение сегнирированный код</param>
        /// <param name="errorAction">При ошибки</param>
        public void Create(string mobileguid, Action<string> messageAction, Action<string> errorAction)
        {
            var parametres = new Dictionary<string, string>() {
                { nameof(mobileguid), mobileguid },
                { "date", DateTime.Now.ToString("yyyy-MM-dd hh:mm") }
            };

            fantom.Create<UserWin>(UserWin.Method, parametres, (msg, msgType) =>
            {

                switch (msgType)
                {
                    case MessageTypes.Done:
                        messageAction(msg);
                        break;
                    default:
                        errorAction(msg);
                        break;
                }
            });
        }

        /// <summary>
        /// Получим по идентификатору устройства, информацию
        /// </summary>
        /// <param name="mobileguid">Идентификатор устройства</param>
        /// <param name="userwinAction">При успехи</param>
        /// <param name="errorAction">При ошибки</param>
        public void Get(string mobileguid, Action<UserWin> userwinAction, Action<string> errorAction)
        {
            fantom.Get<UserWin>(UserWin.Method, new object[] { mobileguid }, (userWin, msgType, msg) =>
            {
                switch (msgType)
                {
                    case MessageTypes.Done:
                        userwinAction(userWin);
                        break;
                    default:
                        errorAction(msg);
                        break;
                }
            });
        }
    }
}
