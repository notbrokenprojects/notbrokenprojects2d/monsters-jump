﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Backend.Enums
{
    /// <summary>
    /// Тип сообщения
    /// </summary>
    public enum MessageTypes
    {
        /// <summary>
        /// Успех
        /// </summary>
        Done = 0,

        /// <summary>
        /// Ошибка
        /// </summary>
        Error = 1
    }
}
