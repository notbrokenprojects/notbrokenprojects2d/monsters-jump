﻿using Assets.Backend.Enums;
using Newtonsoft.Json;
using Proyecto26;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Backend
{
    /// <summary>
    /// Класс для работы с api
    /// </summary>
    public class FantomHH
    {
        /// <summary>
        /// Ссылка на api
        /// </summary>
        private string baseUrl = "https://byskynet.ru/api";
        private static FantomHH instance;

        private FantomHH() { }

        public static FantomHH GetInstance()
        {
            return instance ?? (instance = new FantomHH());
        }

        public static FantomHH GetInstance(string url)
        {
            if (instance != null) instance.baseUrl = url;
            else instance = new FantomHH(url);

            return instance;
        }

        /// <summary>
        /// инициализация
        /// </summary>
        /// <param name="url">Ссылка на  api</param>
        public FantomHH(string url)
        {
            baseUrl = url;
        }

        /// <summary>
        /// Создать объект
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="method">метод</param>
        /// <param name="parametres">Параметры</param>
        /// <param name="action">Функция обработчик</param>
        public void Create<T>(string method, Dictionary<string, string> parametres, Action<string, MessageTypes> action)
        {
            parametres = CheckEmpty(parametres);
            RestClient.Post(new RequestHelper
            {
                Uri = GetUrlMethod(method),
                SimpleForm = parametres
            })
            .Then(response => action(response.Text, MessageTypes.Done))
            .Catch(err => action(FantomHH.GetErrorsMessage(err), MessageTypes.Error));
        }

        /// <summary>
        /// Убрать пустые параметры
        /// </summary>
        /// <param name="parametres">Параметры</param>
        /// <returns>Новые параметры</returns>
        private Dictionary<string, string> CheckEmpty(Dictionary<string, string> parametres)
        {
            return parametres.Where(w => !string.IsNullOrEmpty(w.Value)).ToDictionary(k => k.Key, v => v.Value);
        }

        /// <summary>
        /// Обновить запись
        /// </summary>
        /// <param name="method">Метод</param>
        /// <param name="ids">Ид</param>
        /// <param name="paramteres">параметры</param>
        /// <param name="action">Функция обработчик</param>
        public void Update(string method, object[] ids, Dictionary<string, string> paramteres, Action<string, MessageTypes> action)
        {
            RestClient.Put(new RequestHelper
            {
                Uri = GetUrlMethod(method, ids),
                Params = CheckEmpty(paramteres)
            })
            .Then(response => action(response.Text, MessageTypes.Done))
            .Catch(err => action(FantomHH.GetErrorsMessage(err), MessageTypes.Error));
        }

        /// <summary>
        /// Удалить запись
        /// </summary>
        /// <param name="method">Метод</param>
        /// <param name="ids">Ид</param>
        /// <param name="action">Функция обработчик</param>
        public void Delete(string method, object[] ids, Action<string, MessageTypes> action)
        {
            RestClient.Delete(new RequestHelper
            {
                Uri = GetUrlMethod(method, ids)
            })
            .Then(response => action(response.Text, MessageTypes.Done))
            .Catch(err => action(FantomHH.GetErrorsMessage(err), MessageTypes.Error));
        }

        /// <summary>
        /// Получить список записей
        /// </summary>
        /// <typeparam name="T">Объект</typeparam>
        /// <param name="method">Метод</param>
        /// <param name="action">Функция обработчик</param>
        public void GetList<T>(string method, Action<List<T>, MessageTypes, string> action)
        {
            Get(GetUrlMethod(method), action);
        }

        /// <summary>
        /// Получение конкретной записи
        /// </summary>
        /// <typeparam name="T">Объект</typeparam>
        /// <param name="method">Метод</param>
        /// <param name="ids">Ид параметры</param>
        /// <param name="action">Функция обработчик</param>
        public void Get<T>(string method, object[] ids, Action<T, MessageTypes, string> action)
        {
            Get(GetUrlMethod(method, ids), action);
        }

        /// <summary>
        /// Получить данные и передать в функцию
        /// </summary>
        /// <param name="action">Функция обработчик</param>
        /// <param name="url">Ссылка</param>
        /// <typeparam name="T">Тип данных</typeparam>
        private static void Get<T>(string url, Action<T, MessageTypes, string> action)
        {
            RestClient.Get(url).Then(response =>
            {
                var result = JsonConvert.DeserializeObject<T>(response.Text);
                action(result, MessageTypes.Done, string.Empty);
            }).Catch(err =>
            {
                action(default, MessageTypes.Error, FantomHH.GetErrorsMessage(err));
            });
        }

        /// <summary>
        /// Вернуть текст ошибки
        /// </summary>
        /// <param name="err">Ошибка</param>
        private static string GetErrorsMessage(Exception err)
        {
            var error = (RequestException)err;
            return error.Response;
        }

        /// <summary>
        /// Собрать ссылку на метод метод
        /// </summary>
        /// <param name="method">Метод</param>
        /// <param name="ids">Ид параметры</param>
        /// <returns>Ссылка</returns>
        private string GetUrlMethod(string method, object[] ids = null)
        {
            var url = $"{baseUrl}/{method}";
            if (ids != null && ids.Length > 0) url = $"{url}/{string.Join("/", ids)}";
            return url;
        }
    }
}
