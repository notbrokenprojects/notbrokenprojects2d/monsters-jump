﻿using Newtonsoft.Json;

namespace Assets.Backend.Classes
{
    public class ServerException
    {
        /// <summary>
        /// Получить десериализованный объект
        /// </summary>
        /// <param name="errorMessage">Ответ об ошибке от сервера</param>
        /// <returns></returns>
        public static ServerException GetException(string errorMessage)
        {
            return JsonConvert.DeserializeObject<ServerException>(errorMessage);
        }

        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }

        public enum ExceptionCodes
        {
            NOT_FOUND = 404
        }
    }
}
