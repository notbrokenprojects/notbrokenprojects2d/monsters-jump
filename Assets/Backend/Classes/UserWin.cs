﻿using Newtonsoft.Json;
using System;

namespace Assets.Backend.Classes
{
    public class UserWin
    {
        /// <summary>
        /// Метод в api
        /// </summary>
        public static string Method { get; } = "userswins";

        /// <summary>
        /// ИД
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор устройства
        /// </summary>
        [JsonProperty("mobileguid")]
        public string Mobileguid { get; set; }

        /// <summary>
        /// Код
        /// </summary>
        [JsonProperty("code")]
        public string Code { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        /// <summary>
        /// Получил подарок
        /// </summary>
        [JsonProperty("isGet")]
        public bool IsGet { get; set; }
    }
}
