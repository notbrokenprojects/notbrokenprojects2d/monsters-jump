using Assets.Scripts;
using UnityEngine;
using static Assets.Scripts.Consts.Platforms;

public class Platform : RatelWrapper
{
    [SerializeField]
    Chance chance;

    //���� ���� ��� ��������� ��������
    [SerializeField]
    bool movable;

    bool isLeft;

    internal Chance Chance { get => chance; set => chance = value; }

    // Update is called once per frame
    void Update()
    {
        if (!GlobalContainer.ScreenPoints.CheckPointEntryScreenY(transform.position))
            Destroy(gameObject);

        if (movable)
        {
            if (isLeft)
            {
                transform.Translate(Vector2.left * Consts.Platforms.MOVABLE_PLATFORM_SPEED * Time.deltaTime);
                if (transform.position.x < GlobalContainer.ScreenPoints.LeftBot.position.x)
                    isLeft = false;
            }
            else
            {
                transform.Translate(Vector2.right * Consts.Platforms.MOVABLE_PLATFORM_SPEED * Time.deltaTime);
                if (transform.position.x > GlobalContainer.ScreenPoints.RightBot.position.x)
                    isLeft = true;
            }

        }
    }
}
