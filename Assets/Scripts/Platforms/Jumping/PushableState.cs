﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Platforms
{
    public abstract class PushableState
    {
        GameObject platform;
        Animator animator;
        GlobalContainer globalContainer;
        public GlobalContainer GlobalContainer { get => globalContainer; set => globalContainer = value; }
        public GameObject Platform { get => platform; set => platform = value; }

        public PushableState(GameObject _platform, GlobalContainer _globalContainer)
        {
            platform = _platform;
            animator = Platform.GetComponent<Animator>();
            globalContainer = _globalContainer;
        }

        /// <summary>
        /// Меняем анимацию платформы
        /// </summary>
        /// <param name="state"></param>
        public void ChangeAnimation(int state)
        {
            animator.SetInteger("State", state);
        }

        public abstract IEnumerator Push();
    }
}
