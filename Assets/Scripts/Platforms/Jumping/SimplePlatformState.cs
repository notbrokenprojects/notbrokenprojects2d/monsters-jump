﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Platforms
{
    public class SimplePlatformState : PushableState
    {

        public SimplePlatformState(GameObject _platform, GlobalContainer _globalContainer) : base(_platform, _globalContainer)
        {

        }

        public override IEnumerator Push()
        {
            // прыжок, даем силу толчка
            GlobalContainer.Player.RigidBody.velocity = new Vector2(GlobalContainer.Player.RigidBody.velocity.x, Consts.Player.JUMP_HEIGHT);

            // показываем бум
            GlobalContainer.Up_label.SetActive(true);
            yield return new WaitForSeconds(Consts.Player.JUMP_LABEL_DELAY);
            GlobalContainer.Up_label.SetActive(false);

            GlobalContainer.SoundEffector.PlayJumpSound();
            // ожидаем пока летит игрок
            yield return new WaitForSeconds(Consts.Player.JUMP_DELAY);
            // теперь дабы неумножать скорость прыжка обнулим его
            GlobalContainer.Player.RigidBody.velocity = new Vector2(GlobalContainer.Player.RigidBody.velocity.x, 0);
            yield return new WaitForSeconds(Consts.Player.NEXT_JUMP_DELAY);
        }
    }
}
