﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Platforms.Jumping
{
    public class CrashPlatformState : PushableState
    {

        public CrashPlatformState(GameObject _platform, GlobalContainer _globalContainer) : base(_platform, _globalContainer)
        {
        }

        public override IEnumerator Push()
        {
            ChangeAnimation(Consts.Platforms.Animations.PUSH_STATE);
            yield return new WaitForSeconds(0.1f);
            Platform.GetComponent<EdgeCollider2D>().isTrigger = true;
            GlobalContainer.SoundEffector.PlayWoodCrashSound();

            // ожидаем пока проигрывается анимация
            yield return new WaitForSeconds(Consts.Platforms.CRASH_PLATFORM_DESTROY_DELAY);

            // данная платформа позволяет прыгнуть на нее лишь один раз
            Object.Destroy(Platform);
        }
    }
}
