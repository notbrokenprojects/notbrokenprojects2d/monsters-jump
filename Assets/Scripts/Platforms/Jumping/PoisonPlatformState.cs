﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Platforms
{
    public class PoisonPlatformState : PushableState
    {
        public PoisonPlatformState(GameObject _platform, GlobalContainer _globalContainer) : base(_platform, _globalContainer)
        {
        }

        public override IEnumerator Push()
        {
            // прыжок, даем силу толчка
            GlobalContainer.Player.RigidBody.velocity = new Vector2(GlobalContainer.Player.RigidBody.velocity.x, Consts.Player.JUMP_HEIGHT);
            GlobalContainer.SoundEffector.PlayPoisonSound();
            GlobalContainer.Player.StartCoroutine(PlayerPoisonEffect());
            // уменьшаем хп и ехп при попадании на эту платформу
            GlobalContainer.LevelController.Health--;
            GlobalContainer.LevelController.Exp--;
            // ожидаем пока летит игрок
            yield return new WaitForSeconds(Consts.Player.JUMP_DELAY);
            // теперь дабы неумножать скорость прыжка обнулим его
            GlobalContainer.Player.RigidBody.velocity = new Vector2(GlobalContainer.Player.RigidBody.velocity.x, 0);
            yield return new WaitForSeconds(Consts.Player.NEXT_JUMP_DELAY);
        }

        /// <summary>
        /// Эффект отравления
        /// </summary>
        /// <returns></returns>
        IEnumerator PlayerPoisonEffect()
        {
            Color oldColor = GlobalContainer.PoisonEffect.color;
            GlobalContainer.PoisonEffect.color = new Color(oldColor.r, oldColor.g, oldColor.b, 0.5f);
            yield return new WaitForSeconds(Consts.Effects.POISON_EFFECT_DELAY);
            GlobalContainer.PoisonEffect.color = new Color(oldColor.r, oldColor.g, oldColor.b, 0f);
        }
    }
}

