﻿using UnityEngine;

namespace Assets.Scripts.Platforms.Jumping
{
    public class GatePlatform : RatelWrapper
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            // ворота перемещают рандомно в любую точку экрана
            if (collision.tag == Consts.Player.PLAYER_TAG)
            {
                GlobalContainer.SoundEffector.PlayWindSound();
                GlobalContainer.Player.transform.position = new Vector3(GlobalFuncs.GetRandomXPoint(GlobalContainer),
                    GlobalFuncs.GetRandomYPoint(GlobalContainer),
                    GlobalContainer.Player.transform.position.z);
            }
        }
    }
}
