﻿using UnityEngine;

namespace Assets.Scripts.Platforms.Crashing
{
    public class IcePlatform : RatelWrapper
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            // значит лед ударился
            if (collision.tag == Consts.Player.PLAYER_TAG)
            {
                GlobalContainer.SoundEffector.PlayIceCrashSound();
                GlobalContainer.LevelController.Health = 0;
                Destroy(gameObject.transform.parent.gameObject);
            }
        }

        void Update()
        {
            transform.parent.Translate(Vector2.down * Consts.Enemies.ICE_PLATFORM_MOVE_SPEED * Time.deltaTime);
        }
    }
}
