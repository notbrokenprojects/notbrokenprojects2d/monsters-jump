using Assets.Scripts;
using Assets.Scripts.Bonuses;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static Assets.Scripts.Consts.Platforms;

public class PlatformSpawner : RatelWrapper
{
    public List<Platform> platformPrefabs = new List<Platform>(); //a list of all monsters in the asset

    float currentYpoint = 0;
    List<Platform> commonPlatforms, bronzePlatforms, legendaryPlatforms, rarePlatforms;

    [SerializeField]
    List<Bonus> bonuses;

    [SerializeField]
    List<Enemy> enemies;

    // Start is called before the first frame update
    void Start()
    {
        currentYpoint = GlobalContainer.ScreenPoints.RightTop.position.y;

        commonPlatforms = platformPrefabs.Where(p => p.Chance == Chance.Common).ToList();
        bronzePlatforms = platformPrefabs.Where(p => p.Chance == Chance.Bronze).ToList();
        rarePlatforms = platformPrefabs.Where(p => p.Chance == Chance.Rare).ToList();
        legendaryPlatforms = platformPrefabs.Where(p => p.Chance == Chance.Legendary).ToList();

        StartCoroutine(GeneratePlatforms());
    }

    IEnumerator GeneratePlatforms()
    {
        // �������������� ���������
  
        var newPoint = new Vector3(GlobalFuncs.GetRandomXPoint(GlobalContainer), currentYpoint, 0);    
        var newPlatformPrefab = GetNewPlatform();
        var newPlatform = Instantiate(newPlatformPrefab, newPoint, newPlatformPrefab.transform.rotation, transform);

        // ���� ��� ����� ������ ��������� ����������� �����
        if (isBusy(newPlatform.transform))
        {
            // ����������� ������� ������� �� �, ��� ��������� ���������
            currentYpoint += MIN_Y_OFFSET;
            newPlatform.transform.position = new Vector3(GlobalFuncs.GetRandomXPoint(GlobalContainer), currentYpoint, 0);
        }

        newPlatform.transform.localPosition = new Vector3(newPlatform.transform.localPosition.x, newPlatform.transform.localPosition.y, 0);

        if (!GenerateEnemy(newPlatform))
            GenerateBonus(newPlatform);

        // ����������� ������� ������� �� �, ��� ��������� ���������
        currentYpoint += Random.Range(MIN_Y_OFFSET, MAX_Y_OFFSET);

        yield return new WaitForSeconds(GENERATE_DELAY);

        StartCoroutine(GeneratePlatforms());
    }

    /// <summary>
    /// ����������� ����� � ���������, ���� �������
    /// </summary>
    /// <param name="platform"></param>
    bool GenerateBonus(Platform platform)
    {
        int chance = Random.Range(0, 100) + 1;
        var successArray = bonuses.Where(b => b.Chance >= chance).ToArray();
        
        if (successArray.Length == 0)
            return false;

        var newBonusPrefab = successArray[Random.Range(0, successArray.Length)];
        if (newBonusPrefab != null)         
            Instantiate(newBonusPrefab, platform.transform);

        return newBonusPrefab != null;
    }

    /// <summary>
    /// ����������� ����� � ���������, ���� �������
    /// </summary>
    /// <param name="platform"></param>
    bool GenerateEnemy(Platform platform)
    {
        int chance = Random.Range(0, 100) + 1;
        var successArray = enemies.Where(b => b.Chance >= chance).ToArray();

        if (successArray.Length == 0)
            return false;

        var newBonusPrefab = successArray[Random.Range(0, successArray.Length)];
        if (newBonusPrefab != null)
            Instantiate(newBonusPrefab, platform.transform);

        return newBonusPrefab != null;
    }

    /// <summary>
    /// ������������� ����� ��������� ������ �� �����
    /// </summary>
    /// <returns></returns>
    Platform GetNewPlatform()
    {
        int chance = Random.Range(0, 100) + 1;

        // � ������ ������� ���� �� ������� ��������� ����� �����
        float difficultOffset = (GlobalContainer.LevelController.CurrentLevel / 2) * 0.1f;
        if (chance <= LEGENDARY_PLATFORM_CHANCE + difficultOffset / 2 && legendaryPlatforms.Count > 0)
            return legendaryPlatforms[Random.Range(0, legendaryPlatforms.Count)];
        else if (chance <= RARE_PLATFORM_CHANCE + difficultOffset / 2 && rarePlatforms.Count > 0)
            return rarePlatforms[Random.Range(0, rarePlatforms.Count)];
        else if (chance <= BRONZE_PLATFORM_CHANCE + difficultOffset && bronzePlatforms.Count > 0)
            return bronzePlatforms[Random.Range(0, bronzePlatforms.Count)];
        else
            return commonPlatforms[Random.Range(0, commonPlatforms.Count)];
    }
 
    public bool isBusy(Transform _transform)
    {

        Collider2D[] colliders = Physics2D.OverlapCircleAll(_transform.position, 1f);

        return colliders.Length > 0;
    }
}
