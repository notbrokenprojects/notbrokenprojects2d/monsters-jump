﻿using UnityEngine;

namespace Assets.Scripts
{

    public abstract class RatelWrapper : MonoBehaviour
    {
        GlobalContainer globalContainer;

        public GlobalContainer GlobalContainer 
        {
            get 
            { 
                if (globalContainer == null)
                    globalContainer = GlobalFuncs.GetComponentObject<GlobalContainer>(Consts.Global.GLOBAL_CONTAINER_TAG);

                return globalContainer;
            }
            set => globalContainer = value; 
        
        }

        private void Awake()
        {
            globalContainer = GlobalFuncs.GetComponentObject<GlobalContainer>(Consts.Global.GLOBAL_CONTAINER_TAG);
        }
    }
}
