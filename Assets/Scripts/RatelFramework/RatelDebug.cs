﻿using UnityEngine;

namespace Assets.Scripts
{
    class RatelDebug
    {
        public static void LogError(string message)
        {
            if (Debug.isDebugBuild)
                Debug.LogError(message);
        }

        public static void Log(string message)
        {
            if (Debug.isDebugBuild)
                Debug.Log(message);
        }
    }
}
