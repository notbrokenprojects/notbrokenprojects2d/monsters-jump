using Assets.Scripts;
using Assets.Scripts.Bonuses;
using UnityEngine;

public class Heart : Bonus
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // ������ �������� ���������
        if (collision.tag == Consts.Player.PLAYER_TAG)
        {
            GlobalContainer.SoundEffector.PlayHeartSound();
            GlobalContainer.LevelController.Health++;
            Destroy(gameObject);
        }
    }
}
