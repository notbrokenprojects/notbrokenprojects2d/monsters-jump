using Assets.Scripts;
using Assets.Scripts.Bonuses;
using System.Collections;
using UnityEngine;

public class Shield : Bonus
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // ������ ��� ���������
        if (collision.tag == Consts.Player.PLAYER_TAG)
        {
            GlobalContainer.SoundEffector.PlayShieldSound();
            GlobalContainer.Player.StartCoroutine(ShieldActive(GlobalContainer));
            Destroy(gameObject); 
        }
    }

    /// <summary>
    ///  ������������ ��������� ������
    /// </summary>
    /// <param name="globalContainer"></param>
    /// <returns></returns>
    public static IEnumerator ShieldActive(GlobalContainer globalContainer)
    {
        // �������� ������ ����
        globalContainer.LevelController.IsProtected = true;
        Color oldColor = globalContainer.ShieldEffect.color;
        globalContainer.ShieldEffect.color = new Color(oldColor.r, oldColor.g, oldColor.b, 0.5f);
        yield return new WaitForSeconds(Consts.Effects.SHIELD_EFFECT_DELAY);

        if (!globalContainer.Player.IsDebug)
        {
            //������� ������
            globalContainer.LevelController.IsProtected = false;
            globalContainer.ShieldEffect.color = new Color(oldColor.r, oldColor.g, oldColor.b, 0f);
        }     
    }
}
