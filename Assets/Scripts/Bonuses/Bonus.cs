﻿using UnityEngine;

namespace Assets.Scripts.Bonuses
{
    public class Bonus : RatelWrapper
    {
        public virtual float Chance { get => Consts.Bonuses.BONUS_GENERATION_CHANCE; }

        void Update()
        {
            transform.rotation = Quaternion.AngleAxis(Time.deltaTime * 45, Vector3.up) * transform.rotation;
        }
    }
}
