﻿using Assets.Scripts;
using Assets.Scripts.Bonuses;
using System.Collections;
using UnityEngine;

public class SlowTime : Bonus
{
    float slowTimeScale = 0.7f;
    float oldTimeScale, oldFixedDeltaTime;
    private void Start()
    {
        oldTimeScale = Time.timeScale;
        oldFixedDeltaTime = Time.fixedDeltaTime;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // значит замедление подобрали
        if (collision.tag == Consts.Player.PLAYER_TAG)
        {
            GlobalContainer.SoundEffector.PlaySlowTimeSound();
            GlobalContainer.Player.StartCoroutine(SlowTimeTask());
            gameObject.SetActive(false);
        }
    }

    public IEnumerator SlowTimeTask()
    {
        Time.timeScale = slowTimeScale;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        yield return new WaitForSeconds(Consts.Bonuses.SLOW_TIME_DELAY);

        // после окончание бонуса вернуть скорость
        Time.timeScale = oldTimeScale;
        Time.fixedDeltaTime = oldFixedDeltaTime;
        RatelDebug.Log(Time.timeScale.ToString());
        RatelDebug.Log(Time.fixedDeltaTime.ToString());
    }
}

