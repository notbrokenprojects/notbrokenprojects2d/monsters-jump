using Assets.Scripts;
using System.Collections;
using UnityEngine;

public class PlayerAttack : RatelWrapper
{

    [SerializeField]
    GameObject bulletPrefab;

    public void CreateBullet()
    {
        var newBullet = Instantiate(bulletPrefab);
        newBullet.transform.position = GlobalContainer.StartBulletPoint.position;
    }

    public void Attack()
    {
        if (!GlobalContainer.Player.IsAttacker)
        {
            StartCoroutine(AttackTask());
        }

    }

    IEnumerator AttackTask()
    {
        GlobalContainer.Player.IsAttacker = true;
        GlobalContainer.Player.ChangeAnimation(Consts.AnimationStates.ATTACK_STATE);
        GlobalContainer.SoundEffector.PlayMagicAttack();
        CreateBullet();
        yield return new WaitForSeconds(Consts.Player.ATTACK_DELAY);
        GlobalContainer.Player.IsAttacker = false;
    }
}
