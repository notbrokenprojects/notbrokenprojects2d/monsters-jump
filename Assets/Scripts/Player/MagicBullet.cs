using Assets.Scripts;
using System.Collections;
using UnityEngine;

public class MagicBullet : RatelWrapper
{
    Animator animator;
    Player.Direction direction;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        StartCoroutine(InitState());
        direction = GlobalContainer.Player.CurrentDirection;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GlobalContainer.ScreenPoints.CheckPointEntryScreen(transform.position))
            Destroy(gameObject); 

        if (direction == Player.Direction.Left)
            transform.Translate(Vector2.left * Consts.Player.BULLET_SPEED * Time.deltaTime);
        else
            transform.Translate(Vector2.right * Consts.Player.BULLET_SPEED * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        CollisionCheck(collision.gameObject);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        CollisionCheck(collision.gameObject);
    }

    public void CollisionCheck(GameObject collision)
    {
        if (collision.tag == Consts.Enemies.ENEMY_TAG)
        {
            GlobalContainer.SoundEffector.PlayMonsterDeath();
            GlobalContainer.LevelController.Exp+=2;
            Destroy(collision); 

            if (collision.gameObject.tag != Consts.Player.PLAYER_TAG)
                StartCoroutine(DestroyState());
        }
    }

    /// <summary>
    /// ������������� ����
    /// </summary>
    /// <returns></returns>
    IEnumerator InitState()
    {
        ChangeAnimation(Consts.Player.BulletAnimations.DESTROY_STATE);
        yield return new WaitForSeconds(Consts.Effects.BULLET_INIT_DELAY);
        ChangeAnimation(Consts.Player.BulletAnimations.IDLE_STATE);
    }

    /// <summary>
    /// ����������� ����
    /// </summary>
    /// <returns></returns>
    IEnumerator DestroyState()
    {
        ChangeAnimation(Consts.Player.BulletAnimations.DESTROY_STATE);
        yield return new WaitForSeconds(Consts.Effects.BULLET_DESTROY_DELAY);
        Destroy(gameObject);
    }

    /// <summary>
    /// ������ �������� ���������
    /// </summary>
    /// <param name="state"></param>
    public void ChangeAnimation(int state)
    {
        animator.SetInteger("State", state);
    }
}
