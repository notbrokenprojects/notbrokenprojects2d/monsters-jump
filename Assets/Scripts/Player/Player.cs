using Assets.Scripts;
using Assets.Scripts.Platforms;
using Spine.Unity;
using System;
using System.Collections;
using UnityEngine;

public class Player : RatelWrapper
{

    Rigidbody2D rigidBody;

    [SerializeField]
    private bool isJumped, isAttacker, isCrashed, isDebug;

    bool isDeath;

    private GameObject currentMonster;
    SkeletonAnimation monsterAnimator; //The animator script of the monster
    GlobalContainer globalContainer;
    PushableState jumpState;
    Direction direction;

    public Rigidbody2D RigidBody { get => rigidBody; set => rigidBody = value; }

    /// <summary>
    /// ���� ��� ����������� ����������� �������� ������, � ������� ��� ��������, � �� ��������� ��� ������������
    /// </summary>
    public Direction ControlDirection
    {
        get
        {
            float offsetH = 0;
            if (Application.platform == RuntimePlatform.Android)
                offsetH = Input.acceleration.x;
            else
                offsetH = globalContainer.Joystick.Horizontal;

            if (offsetH >= Consts.Player.ACCELERATION_OFFSET)
                return Direction.Right;
            else if (offsetH <= -Consts.Player.ACCELERATION_OFFSET)
                return Direction.Left;
            else
                return Direction.Idle;
        }
    }

    public GameObject CurrentMonster
    {
        get => currentMonster;
        set
        {

            // �������� ��������� �������
            var monsterPosition = currentMonster.transform.position;
            var monsterScale = currentMonster.transform.localScale;
            var oldMonster = currentMonster;

            currentMonster = Instantiate(value, transform);
            currentMonster.transform.position = monsterPosition;
            currentMonster.transform.localScale = monsterScale;

            monsterAnimator = currentMonster.GetComponent<SkeletonAnimation>();

            // ��������� ������� �������
            Destroy(oldMonster);
        }
    }

    public bool IsDeath { get => isDeath; set => isDeath = value; }
    public bool IsAttacker { get => isAttacker; set => isAttacker = value; }
    public Direction CurrentDirection { get => direction; set => direction = value; }
    public PushableState JumpState { get => jumpState; set => jumpState = value; }
    public bool IsJumped { get => isJumped; set => isJumped = value; }
    public bool IsDebug { get => isDebug; set => isDebug = value; }

    // Start is called before the first frame update
    void Start()
    {
        try
        {
            rigidBody = GetComponent<Rigidbody2D>();
            currentMonster = transform.GetChild(transform.childCount - 1).gameObject;
            monsterAnimator = currentMonster.GetComponent<SkeletonAnimation>();
            globalContainer = GlobalFuncs.GetComponentObject<GlobalContainer>(Consts.Global.GLOBAL_CONTAINER_TAG);

            ChangeAnimation(Consts.AnimationStates.IDLE_STATE);

            StartCoroutine(JumpTask());

            if (Application.platform == RuntimePlatform.Android)
                globalContainer.Joystick.gameObject.SetActive(false);

            globalContainer.Up_label.SetActive(false);
            direction = Direction.Right;

            if (isDebug)
                StartCoroutine(Shield.ShieldActive(globalContainer));
        }
        catch (Exception e)
        {
            RatelDebug.LogError($"��������� ������ ��� ������������� ��������� ���������� ������ {e.Message}");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (IsDeath)
            transform.Translate(Vector2.down * Consts.Player.MOVE_SPEED * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        if (!isDeath)
        {
            // ����������� ���������
            jumpState = globalContainer.GroundCheck.GetPushState(globalContainer);
            isJumped = jumpState != null ? true : false;

            Flip();
            if (ControlDirection == Direction.Right)
                RigidBody.velocity = new Vector2(Consts.Player.MOVE_SPEED, RigidBody.velocity.y);
            else if (ControlDirection == Direction.Left)
                RigidBody.velocity = new Vector2(-Consts.Player.MOVE_SPEED, RigidBody.velocity.y);
            else
                RigidBody.velocity = new Vector2(0f, RigidBody.velocity.y);
        }

        // �������� ���������� ������
        globalContainer.LevelController.UpdateElevation();
    }

    void Flip()
    {
        // ���� ������ ������
        if (ControlDirection == Direction.Right)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);

            // ������ �������� ���� ���� �������� �� Z
            Vector3 localPosition = globalContainer.Player_flip_layer.transform.localPosition;
            globalContainer.Player_flip_layer.localPosition = new Vector3(localPosition.x, localPosition.y, -1);
            direction = Direction.Right;
        }
        if (ControlDirection == Direction.Left)
        {
            transform.localRotation = Quaternion.Euler(0, 180, 0);

            // ������ �������� ���� ���� �������� �� Z
            Vector3 localPosition = globalContainer.Player_flip_layer.transform.localPosition;
            globalContainer.Player_flip_layer.localPosition = new Vector3(localPosition.x, localPosition.y, 1);
            direction = Direction.Left;
        }

    }

    IEnumerator JumpTask()
    {
        yield return null;
        if (isJumped)
        {
            ChangeAnimation(Consts.AnimationStates.WALK_STATE);     

            yield return StartCoroutine(jumpState.Push());
          
            isJumped = false;
            ChangeAnimation(Consts.AnimationStates.IDLE_STATE);
        }

        StartCoroutine(JumpTask());
    }

    public void ChangeAnimation(string newAnimationName)  //Names are: Idle, Walk, Dead and Attack
    {
        if (monsterAnimator == null)
            return;

        // �������� ����� ������ ������������
        if (!isAttacker || newAnimationName == Consts.AnimationStates.DEAD_STATE || newAnimationName == Consts.AnimationStates.ATTACK_STATE)
        {
            bool IsLoop = true;
            if (newAnimationName == Consts.AnimationStates.ATTACK_STATE)
                IsLoop = false;

            //set the animation state to the selected one
            monsterAnimator.AnimationState.SetAnimation(0, newAnimationName, IsLoop);
        }       
    }

    public enum Direction
    {
        Left,
        Right,
        Idle
    }
}
