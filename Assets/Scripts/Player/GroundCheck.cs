using Assets.Scripts;
using Assets.Scripts.Platforms;
using Assets.Scripts.Platforms.Jumping;
using System.Linq;
using UnityEngine;

/// <summary>
/// ����������� ����� ��� ������ �� ���������� �� �����
/// </summary>
public class GroundCheck : RatelWrapper
{
    /// <summary>
    /// �������� ������� ��������� �� ������� ����������� �����
    /// </summary>
    /// <param name="globalContainer"></param>
    /// <returns></returns>
    public PushableState GetPushState(GlobalContainer globalContainer)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.3f).Where(p => p.gameObject.GetComponent<Platform>() != null).ToArray();
        if (colliders.Length == 0)
            return null;

        var collision = colliders.FirstOrDefault().gameObject;
        switch (collision.tag)
        {
            case Consts.Platforms.JUMP_PLATFORM_TAG:
                return new JumpPlatformState(collision, globalContainer);
            case Consts.Platforms.SIMPLE_PLATFORM_TAG:
                return new SimplePlatformState(collision, globalContainer);
            case Consts.Platforms.POISON_PLATFORM_TAG:
                return new PoisonPlatformState(collision, globalContainer);
            case Consts.Platforms.CRASH_PLATFORM_TAG:
                return new CrashPlatformState(collision, globalContainer);
            default:
                return null;
        }
    }
}
