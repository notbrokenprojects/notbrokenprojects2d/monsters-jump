﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    /// Класс вычисления границами экрана
    /// </summary>
    public class ScreenPoints : RatelWrapper
    {
        [SerializeField]
        Transform leftBot, leftTop, rightBot, rightTop;

        public Transform LeftBot { get => leftBot; set => leftBot = value; }
        public Transform RightBot { get => rightBot; set => rightBot = value; }
        public Transform LeftTop{ get => leftTop; set => leftTop = value; }
        public Transform RightTop { get => rightTop; set => rightTop = value; }


        private void Start()
        {
            leftBot.position = GetLeftBotPos(GlobalContainer.Player.transform);
            rightTop.position = GetRightTopPos(GlobalContainer.Player.transform);
            leftTop.position = GetLeftTopPos(GlobalContainer.Player.transform);
            rightBot.position = GetRightBotPos(GlobalContainer.Player.transform);
        }

        public Vector3 GetRightTopPos(Transform sourcePosition)
        {
            return CalcPosition(new Vector2(Screen.width, Screen.height), sourcePosition);
        }

        public Vector3 GetLeftTopPos(Transform sourcePosition)
        {
            return CalcPosition(new Vector2(0f, Screen.height), sourcePosition);
        }

        public Vector3 GetRightBotPos(Transform sourcePosition)
        {
            return CalcPosition(new Vector2(Screen.width, 0f), sourcePosition);
        }

        public Vector3 GetLeftBotPos(Transform sourcePosition)
        {
            return CalcPosition(new Vector2(0f, 0f), sourcePosition);
        }

        /// <summary>
        /// Проверить находится ли точка в пределах видимости экрана
        /// </summary>
        /// <param name="position">исходная позиция</param>
        /// <returns></returns>
        public bool CheckPointEntryScreen(Vector3 position)
        {
            return (position.y > LeftBot.position.y) && (position.x > LeftBot.position.x) && (position.x < RightBot.position.x);
        }

        /// <summary>
        /// Проверить находится ли точка в пределах видимости экрана по Y
        /// </summary>
        /// <param name="position">исходная позиция</param>
        /// <returns></returns>
        public bool CheckPointEntryScreenY(Vector3 position)
        {
            return (position.y > LeftBot.position.y);
        }

        /// <summary>
        /// Расчитать точку в 3д пространстве из Vector2, при помощи этой функции вычисляем точки границы экрана
        /// </summary>
        /// <param name="screenPos"></param>
        /// <param name="sourcePosition">Исходная позиция на сцене в игре от которой ориентируется расчет</param>
        /// <returns></returns>
        public static Vector3 CalcPosition(Vector2 screenPos, Transform sourcePosition)
        {
            Plane plane = new Plane(sourcePosition.forward, sourcePosition.position);

            Ray ray = Camera.main.ScreenPointToRay(screenPos);
            float dist = 0f;
            Vector3 pos = Vector3.zero;

            if (plane.Raycast(ray, out dist))
                pos = ray.GetPoint(dist);

            return pos;
        }
    }
}
