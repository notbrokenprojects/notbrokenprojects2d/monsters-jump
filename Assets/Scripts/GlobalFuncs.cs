﻿using UnityEngine;

namespace Assets.Scripts
{
    class GlobalFuncs
    {

        /// <summary>
        /// Вернуть компонент объекта по тегу
        /// </summary>
        /// <typeparam name="T">Произвольный тип от MonoBehavior</typeparam>
        /// <param name="tag">Тег для поиска объекта на сцене</param>
        /// <returns></returns>
        public static T GetComponentObject<T>(string tag)
        {
            T component = default(T);
            var globalGameObject = GameObject.FindGameObjectWithTag(tag);

            if (globalGameObject == null)  
                RatelDebug.LogError($"Не удалось найти объект с тегом {tag}");

            if (!globalGameObject.TryGetComponent(out component))
                RatelDebug.LogError($"Не удалось найти компонент {nameof(T)} у объекта {globalGameObject.name}");

            return component;
        }

        /// <summary>
        /// Сгенерировать точку по Х в пределах экрана
        /// </summary>
        /// <returns></returns>
        public static float GetRandomXPoint(GlobalContainer globalContainer)
        {
            return UnityEngine.Random.Range(globalContainer.ScreenPoints.LeftTop.position.x, globalContainer.ScreenPoints.RightTop.position.x);
        }

        /// <summary>
        /// Сгенерировать точку по Y в пределах экрана
        /// </summary>
        /// <returns></returns>
        public static float GetRandomYPoint(GlobalContainer globalContainer)
        {
            return UnityEngine.Random.Range(globalContainer.ScreenPoints.RightBot.position.y, globalContainer.ScreenPoints.RightTop.position.y);
        }
    }
}
