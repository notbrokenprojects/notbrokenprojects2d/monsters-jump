﻿namespace Assets.Scripts
{
    class Consts
    {
        public static class Hyperlinks
        {

            public const string RATEL_GAMES_LINK = "https://www.instagram.com/ratelgames/";
            public const string MALVIS_LINK = "https://www.instagram.com/malvislight/";               
        }

        public static class Player
        {
            public const uint MAX_HEALTH = 3;

            public const float DEFAULT_MONSTER_SCALE = 0.5f;
            public const string START_BULLET_POINT_TAG = "BulletPoint";
            public const string GROUND_CHECK_TAG = "GroundCheck";
            public const string JOYSTICK_TAG = "Joystick";
            public const string PLAYER_TAG = "Player";
            public const string HEALTH_BAR_TAG = "HealthSlider";
            public const string EXP_BAR_TAG = "ExpSlider";
            public const string Up_label = "Up_label";
            public const string PLAYER_FLIP_LAYER = "Player_Flip_Layer";

            public const float ACCELERATION_OFFSET = 0.1f;
            public const float JUMP_HEIGHT = 7f;
            public const float MOVE_SPEED = 3f;
            public const float JUMP_LABEL_DELAY = 0.2f;
            public const float JUMP_DELAY = 0.3f;
            public const float ATTACK_DELAY = 1f;
            public const float NEXT_JUMP_DELAY = 0.2f;
            public const float LOSE_DELAY = 2f;

            public const float BULLET_SPEED = 2f;

            // количество высоты которую необходимо преодолеть для получения опыта(exp)
            public const float ELEVATION_EXP_PERIOD = 6f;

            public static class BulletAnimations
            {
                public const int INIT_STATE = 0;
                public const int IDLE_STATE = 1;
                public const int DESTROY_STATE = 2;
            }
        }

        public static class Effects
        {

            public const string PLAYER_POISON_EFFECT_TAG = "PoisonEffect";
            public const float POISON_EFFECT_DELAY = 1f;

            public const string PLAYER_SHIELD_EFFECT_TAG = "ShieldEffect";
            public const float SHIELD_EFFECT_DELAY = 5f;

            public const float BULLET_INIT_DELAY = 0.4f;
            public const float BULLET_DESTROY_DELAY = 0.1f;
        }

        public static class AnimationStates
        {
            public const string DEAD_STATE = "Dead";
            public const string IDLE_STATE = "Idle";
            public const string ATTACK_STATE = "Attack";
            public const string WALK_STATE = "Walk";
        }

        public static class Global
        {
            public const string GLOBAL_CONTAINER_TAG = "GlobalContainer";
            public const string PANEL_GAMEOVER_TAG = "Gameover";
            public const string PANEL_WIN_TAG = "Win";
            public const string LEVEL_CONTROLLER_TAG = "LevelController";
            public const string ELEVATION_TEXT_TAG = "Elevation";
            public const string START_POSITION_TAG = "StartPosition";
            public const string SCREEN_POINTS_TAG = "ScreenPoints";
            public const string GOOGLE_ADS_TAG = "GoogleAds";

            public const int MAX_GAME_LEVEL = 101;

            public const string AD_UNIT_ID = "ca-app-pub-8642237193255733/2633384426";
            public const string AD_UNIT_BANNER_ID = "ca-app-pub-8642237193255733/6263100680";
            public const string AD_TEST_UNIT_ID = "ca-app-pub-3940256099942544/1033173712";
            public const string AD_TEST_BANNER_UNIT_ID = "ca-app-pub-3940256099942544/1033173712";
        }

        public static class Platforms
        {
            public const float GENERATE_DELAY = 0.2f;

            /// <summary>
            /// Максимальное значение на которое увеличиваем позицию по У после каждой генерации новой платформы
            /// </summary>
            public const float MAX_Y_OFFSET = 0.7f;
            /// <summary>
            /// Минимальное значение на которое увеличиваем позицию по У после каждой генерации новой платформы
            /// </summary>
            public const float MIN_Y_OFFSET = 0.2f;
            public const string SIMPLE_PLATFORM_TAG = "SimplePlatform";
            public const string JUMP_PLATFORM_TAG = "JumpPlatform";
            public const string POISON_PLATFORM_TAG = "PoisonPlatform";
            public const string ICE_PLATFORM_TAG = "IcePlatform";
            public const string CRASH_PLATFORM_TAG = "CrashPlatform";
            public const string GATE_PLATFORM_TAG = "GatePlatform";
            public const float CRASH_PLATFORM_DESTROY_DELAY = 0.3f;

            public const float BRONZE_PLATFORM_CHANCE = 15f;
            public const float RARE_PLATFORM_CHANCE = 8f;
            public const float LEGENDARY_PLATFORM_CHANCE = 1f;
            public const float MOVABLE_PLATFORM_SPEED = 1f;
           
            public static class Animations
            {
                public const int IDLE_STATE = 0;
                public const int PUSH_STATE = 1;
            }

            public enum Chance
            {
                Common,
                Bronze,
                Rare,
                Legendary
            }
        }

        public static class Camera
        {
            public const string CAMERA_TAG = "MainCamera";
            public const float DEFAULT_ASPECT = 1440f / 2960f;
            public enum State
            {
                /// <summary>
                /// Режим слежения за игроком
                /// </summary>
                Player,
                /// <summary>
                /// Режим слежения за сценой
                /// </summary>
                Scene
            }
        }

        public static class Monsters
        {
            public const string MONSTERS_SPAWNER_TAG = "MonstersSpawner";
        }

        public static class Enemies
        {
            public const string ENEMY_TAG = "Enemy";
            public const string ENEMY_ATTACK_CHECK_TAG = "AttackCheck";
            public const float ICE_PLATFORM_CRASH_DELAY = 0.3f;
            public const float ICE_PLATFORM_MOVE_SPEED = 2.5f;
            public const float ENEMY_PATROL_FLIP_DELAY = 4f;
            public const float ENEMY_PATROL_ATTACK_DELAY = 1f;
            public const float ENEMY_ANIMATION_ATTACK_DELAY = 1f;
            public const float ENEMY_RESPAWN_CHANCE = 5f;
        }

        public static class Sounds
        {
            public const string SOUND_EFFECTOR_TAG = "SoundEffector";
        }

        public static class Bonuses
        {
            public const float ROTATE_ANIMATION_DELAY = 0.1f;

            /// <summary>
            /// относительно У сдвиг от платформы
            /// </summary>
            public const float Y_OFFSET_TO_PLATFORM = 0.1f;

            public const float SLOW_TIME_DELAY = 5f;
            public const float BONUS_GENERATION_CHANCE = 5f;
        }
    }
}
