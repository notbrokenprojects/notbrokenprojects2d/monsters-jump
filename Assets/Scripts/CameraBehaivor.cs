using Assets.Scripts;
using UnityEngine;
using static Assets.Scripts.Consts.Camera;

public class CameraBehaivor : MonoBehaviour
{
    [SerializeField]
    float timeScale = 1f;

    float speed = 3f;
    public Transform target;
    State curState;

    [SerializeField]
    SpriteRenderer background;

    internal State CurState { get => curState; set => curState = value; }

    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(transform.transform.position.x, target.transform.position.y, transform.position.z);
        curState = State.Scene;
        // ��������� ��� ��� ������� ������
        background.transform.localScale = ChangeLocalScale(background.sprite.rect);
        Time.timeScale = timeScale;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;

        // Disable screen dimming
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = target.position;

        // ���� ������ �� �������, �� ������ ������� ��� �� � �� �
        position.x = (CurState == State.Player) ? target.transform.position.x : transform.position.x;
        position.z = transform.position.z;
        transform.position = Vector3.Lerp(transform.position, position, speed * Time.deltaTime);
    }

    private void Awake()
    {

        float _defaultWidth = Camera.main.orthographicSize * Consts.Camera.DEFAULT_ASPECT;

        //  �������������� ������� ������ � ����� � ��������� ��������� ����������
        Camera.main.orthographicSize = _defaultWidth / Camera.main.aspect;
    }

    /// <summary>
    /// ������ �� �������� ������ ���������� ��������� �������������
    /// </summary>
    /// <param name="spriteRect"></param>
    /// <returns></returns>
    Vector3 ChangeLocalScale(Rect spriteRect)
    {
        float aspectRatio = Screen.width / (float)Screen.height; // in respect to width

        // Guesstimation
        int orthoPixelsY = Mathf.CeilToInt(Camera.main.orthographicSize * 2 * 100);
        int orthoPixelsX = Mathf.CeilToInt(orthoPixelsY * aspectRatio);

        // find the ratio in scales along each respective axis' and assign as the new local scale
        Vector3 newScale = new Vector3(orthoPixelsX / (float)spriteRect.width, orthoPixelsY / (float)spriteRect.height, 1f);
        return newScale;
    }

}

