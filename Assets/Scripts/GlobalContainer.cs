using Assets.Scripts;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalContainer : MonoBehaviour
{
    public List<GameObject> AllMonsters = new List<GameObject>(); //a list of all monsters in the asset
    GroundCheck groundCheck;
    Joystick joystick;
    Player player;
    LevelController levelController;
    CameraBehaivor cameraBehaivor;
    Slider healthBar;
    Slider expBar;
    MonstersSpawner monsterSpawner;
    SpriteRenderer poisonEffect, shieldEffect;

    Text elevationText;
   

    Transform startPosition;
    SoundEffector soundEffector;
    ScreenPoints screenPoints;
    GameObject up_label;
    Transform startBulletPoint;
    Transform player_flip_layer;
    RatelGoogleMobileAds googleAds;

    // ��� ���� ���������� �� �������, ������� �������������� � �������
    [SerializeField]
    GameObject panel_gameover, panel_win, panel_damageEffect;

    [SerializeField]
    Text winCode;

    public GroundCheck GroundCheck { get => groundCheck; }
    public Joystick Joystick { get => joystick; }

    public Player Player { get => player; }
    public CameraBehaivor CameraBehaivor { get => cameraBehaivor; set => cameraBehaivor = value; }
    public LevelController LevelController { get => levelController; set => levelController = value; }
    public Slider HealthBar { get => healthBar; set => healthBar = value; }
    public Slider ExpBar { get => expBar; set => expBar = value; }
    public MonstersSpawner MonsterSpawner { get => monsterSpawner; set => monsterSpawner = value; }
    public SpriteRenderer PoisonEffect { get => poisonEffect; set => poisonEffect = value; }
    public Transform StartPosition { get => startPosition; set => startPosition = value; }
    public Text ElevationText { get => elevationText; set => elevationText = value; }
    public SoundEffector SoundEffector { get => soundEffector; set => soundEffector = value; }
    public SpriteRenderer ShieldEffect { get => shieldEffect; set => shieldEffect = value; }
    public ScreenPoints ScreenPoints { get => screenPoints; set => screenPoints = value; }
    public GameObject Up_label { get => up_label; set => up_label = value; }
    public Transform Player_flip_layer { get => player_flip_layer; set => player_flip_layer = value; }
    public Transform StartBulletPoint { get => startBulletPoint; set => startBulletPoint = value; }
    public GameObject Panel_win { get => panel_win; set => panel_win = value; }
    public GameObject Panel_gameover { get => panel_gameover; set => panel_gameover = value; }
    public RatelGoogleMobileAds GoogleAds { get => googleAds; set => googleAds = value; }
    public Text WinCode { get => winCode; set => winCode = value; }
    public GameObject Panel_damageEffect { get => panel_damageEffect; set => panel_damageEffect = value; }

    // Start is called before the first frame update
    void Awake()
    {
        try
        {
            levelController = GlobalFuncs.GetComponentObject<LevelController>(Consts.Global.LEVEL_CONTROLLER_TAG);
            startPosition = GlobalFuncs.GetComponentObject<Transform>(Consts.Global.START_POSITION_TAG);
            elevationText = GlobalFuncs.GetComponentObject<Text>(Consts.Global.ELEVATION_TEXT_TAG);
            screenPoints = GlobalFuncs.GetComponentObject<ScreenPoints>(Consts.Global.SCREEN_POINTS_TAG);
            googleAds = GlobalFuncs.GetComponentObject<RatelGoogleMobileAds>(Consts.Global.GOOGLE_ADS_TAG);

            groundCheck = GlobalFuncs.GetComponentObject<GroundCheck>(Consts.Player.GROUND_CHECK_TAG);
            joystick = GlobalFuncs.GetComponentObject<FixedJoystick>(Consts.Player.JOYSTICK_TAG);
            player = GlobalFuncs.GetComponentObject<Player>(Consts.Player.PLAYER_TAG);
            healthBar = GlobalFuncs.GetComponentObject<Slider>(Consts.Player.HEALTH_BAR_TAG);
            expBar = GlobalFuncs.GetComponentObject<Slider>(Consts.Player.EXP_BAR_TAG);
            up_label = GlobalFuncs.GetComponentObject<Transform>(Consts.Player.Up_label).gameObject;
            player_flip_layer = GlobalFuncs.GetComponentObject<Transform>(Consts.Player.PLAYER_FLIP_LAYER);
            startBulletPoint = GlobalFuncs.GetComponentObject<Transform>(Consts.Player.START_BULLET_POINT_TAG);

            cameraBehaivor = GlobalFuncs.GetComponentObject<CameraBehaivor>(Consts.Camera.CAMERA_TAG);
            soundEffector = GlobalFuncs.GetComponentObject<SoundEffector>(Consts.Sounds.SOUND_EFFECTOR_TAG);

            monsterSpawner = GlobalFuncs.GetComponentObject<MonstersSpawner>(Consts.Monsters.MONSTERS_SPAWNER_TAG);
            poisonEffect = GlobalFuncs.GetComponentObject<SpriteRenderer>(Consts.Effects.PLAYER_POISON_EFFECT_TAG);
            shieldEffect = GlobalFuncs.GetComponentObject<SpriteRenderer>(Consts.Effects.PLAYER_SHIELD_EFFECT_TAG);                          
        }
        catch (Exception e)
        {
            RatelDebug.LogError($"��������� ������ ��� ������������� ��������� ���������� ���� {e.Message}");
        }
    }
}
