using Assets.Scripts;
using System.Collections;
using UnityEngine;

public class LoseEvent : RatelWrapper
{

    // Update is called once per frame
    void FixedUpdate()
    {
        // ������ ��� ����� ����
        if (transform.position.y < GlobalContainer.ScreenPoints.LeftBot.position.y)
        {
            StartCoroutine(GameoverTask(GlobalContainer));
        }

        // ���������� �� ������ ����� ������ ���� ����� �� ������� �� �
        if (transform.position.x < GlobalContainer.ScreenPoints.LeftBot.position.x)
        {
            transform.position = new Vector3(GlobalContainer.ScreenPoints.RightBot.position.x, transform.position.y, transform.position.z);
        }

        if (transform.position.x > GlobalContainer.ScreenPoints.RightBot.position.x)
        {
            transform.position = new Vector3(GlobalContainer.ScreenPoints.LeftBot.position.x, transform.position.y, transform.position.z);
        }
    }

    public static IEnumerator GameoverTask(GlobalContainer globalContainer)
    {

        // ������������� �������
        globalContainer.Player.RigidBody.bodyType = RigidbodyType2D.Static;
        globalContainer.Player.IsDeath = true;
        globalContainer.SoundEffector.PlayGameOver();
        // ������ ������ � ������
        globalContainer.CameraBehaivor.CurState = Consts.Camera.State.Player;
        globalContainer.Player.ChangeAnimation(Consts.AnimationStates.DEAD_STATE);
        yield return new WaitForSeconds(Consts.Player.LOSE_DELAY);

        globalContainer.Panel_gameover.SetActive(true);
        globalContainer.GoogleAds.LoadBanner();
        globalContainer.LevelController.GameOverText.text = $"{globalContainer.LevelController.MonsterNameText.text}";
    }
}
