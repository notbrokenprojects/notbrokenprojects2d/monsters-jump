using Assets.Scripts;
using GoogleMobileAds.Api;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RatelGoogleMobileAds : MonoBehaviour
{
    private InterstitialAd _interstitialAdBlock;
    private BannerView _bannerView;
    AdRequest request;
    // Start is called before the first frame update
    void Start()
    {
        MobileAds.Initialize(initStatus => { });
        if (Application.platform == RuntimePlatform.Android)
            _interstitialAdBlock = new InterstitialAd(Consts.Global.AD_UNIT_ID);
        else
            _interstitialAdBlock = new InterstitialAd(Consts.Global.AD_TEST_UNIT_ID);

        if (Application.platform == RuntimePlatform.Android)
            _bannerView = new BannerView(Consts.Global.AD_UNIT_BANNER_ID, AdSize.SmartBanner, AdPosition.Top);
        else
            _bannerView = new BannerView(Consts.Global.AD_TEST_BANNER_UNIT_ID, AdSize.SmartBanner, AdPosition.Top);

        // Create an empty ad request.
        request = new AdRequest.Builder().Build();

        // Load the interstitial with the request.
        _interstitialAdBlock.LoadAd(request);
        _interstitialAdBlock.OnAdClosed += HandleRewardedAdClosed;

      

    }

    public void LoadBanner()
    {
        _bannerView.LoadAd(request);
    }

    public void DestroyBanner()
    {
        _bannerView.Destroy();
    }

    /// <summary>
    /// �������� ��������
    /// </summary>
    public void AdBlockShow()
    {
        if (this._interstitialAdBlock.IsLoaded() && RestartCount.restartCount % 2 == 0)
            this._interstitialAdBlock.Show();
        else
            ReloadGame();

        DestroyBanner();
        RestartCount.restartCount++;
    }

    public void HandleRewardedAdClosed(object sender, EventArgs args)
    {
        ReloadGame();
    }

    public void ReloadGame()
    {
        // ��������� ����� ������
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}

public static class RestartCount
{
    public static int restartCount = 0;
}
