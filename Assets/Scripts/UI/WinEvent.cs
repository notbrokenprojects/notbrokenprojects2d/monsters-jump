﻿using Assets.Backend.Services;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class WinEvent : RatelWrapper
    {
        bool isWin;
        // Update is called once per frame
        void FixedUpdate()
        {
            // значит наш игрок упал
            if (GlobalContainer.LevelController.CurrentLevel >= Consts.Global.MAX_GAME_LEVEL && !isWin)
                StartCoroutine(WinTask());
          
        }

        IEnumerator WinTask()
        {
            // останавливаем падение
            GlobalContainer.Player.RigidBody.bodyType = RigidbodyType2D.Static;
            GlobalContainer.SoundEffector.PlayWinGame();
            // камеру ставим у игрока
            GlobalContainer.CameraBehaivor.CurState = Consts.Camera.State.Player;

            

            isWin = true;
            UsersWinsServices userService = new UsersWinsServices();
            userService.Create(SystemInfo.deviceUniqueIdentifier, (winCode) =>
            {
                // GlobalContainer.WinCode.text = SystemInfo.deviceUniqueIdentifier.Substring(1, 10) + (char)Random.Range('A', 'Z' + 1); 
                GlobalContainer.WinCode.text = winCode;
            }, 
            (errorMessage) => { });


            GlobalContainer.Panel_win.SetActive(true);
            yield return null;
        }
    }
}
