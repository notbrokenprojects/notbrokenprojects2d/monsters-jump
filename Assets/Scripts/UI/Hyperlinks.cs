using Assets.Scripts;
using UnityEngine;

public class Hyperlinks : MonoBehaviour
{
    public void OpenRatelGamesLink()
    {
        Application.OpenURL(Consts.Hyperlinks.RATEL_GAMES_LINK);
    }

    public void OpenMalvisLink()
    {
        Application.OpenURL(Consts.Hyperlinks.MALVIS_LINK);
    }
}
