using UnityEngine;

public class SoundEffector : MonoBehaviour
{
    public AudioSource audioSource;

    [SerializeField]
    AudioClip jumpSound, 
        longJumpSound, poisonSound, 
        shieldSound, heartSound, 
        iceCrashSound, woodCrashSound, windSound,
        magicAttackSound, levelUpSound, gameOverSound, 
        monsterDeathSound, winSound, slowTimeSound;

    public void PlayWinGame()
    {
        if (winSound != null)
            audioSource.PlayOneShot(winSound);
    }

    public void PlayMagicAttack()
    {
        if (magicAttackSound != null)
            audioSource.PlayOneShot(magicAttackSound);
    }

    public void PlayLevelUp()
    {
        if (levelUpSound != null)
            audioSource.PlayOneShot(levelUpSound);
    }

    public void PlayGameOver()
    {
        if (gameOverSound != null)
            audioSource.PlayOneShot(gameOverSound);
    }

    public void PlayMonsterDeath()
    {
        if (monsterDeathSound != null)
            audioSource.PlayOneShot(monsterDeathSound);
    }
    public void PlayJumpSound()
    {
        if (jumpSound != null)
            audioSource.PlayOneShot(jumpSound);
    }

    public void PlayLongJumpSound()
    {
        if (longJumpSound != null)
            audioSource.PlayOneShot(longJumpSound);
    }

    public void PlayPoisonSound()
    {
        if (poisonSound != null)
            audioSource.PlayOneShot(poisonSound);
    }

    public void PlayShieldSound()
    {
        if (shieldSound != null)
            audioSource.PlayOneShot(shieldSound);
    }

    public void PlayHeartSound()
    {
        if (heartSound != null)
            audioSource.PlayOneShot(heartSound);
    }

    public void PlaySlowTimeSound()
    {
        if (slowTimeSound != null)
            audioSource.PlayOneShot(slowTimeSound);
    }

    public void PlayIceCrashSound()
    {
        if (iceCrashSound != null)
            audioSource.PlayOneShot(iceCrashSound);
    }

    public void PlayWoodCrashSound()
    {
        if (woodCrashSound != null)
            audioSource.PlayOneShot(woodCrashSound);
    }

    public void PlayWindSound()
    {
        if (windSound != null)
            audioSource.PlayOneShot(windSound);
    }
}
