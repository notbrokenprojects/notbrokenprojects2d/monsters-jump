using Assets.Scripts;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LevelController : RatelWrapper
{
    [SerializeField]
    int currentLevel, expBarMaxValue = 1;
    int exp = 0;
    uint health;
    // lastElevationRecord ��������� ����������� ����� �� ������� ������ exp ������ �� ���������� ������������ ������
    float elevation, elevationRecord, lastElevationRecord;
    bool isProtected;

    [SerializeField]
    Text monsterNameText, gameOverText;

    public Text MonsterNameText { get => monsterNameText; set => monsterNameText = value; }
    public Text GameOverText { get => gameOverText; set => gameOverText = value; }
    public bool IsProtected { get => isProtected; set => isProtected = value; }

    public int CurrentLevel { get => currentLevel; set => currentLevel = value; }

    /// <summary>
    /// ��������� ���������� ������ �������
    /// </summary>
    public void UpdateElevation()
    {
        elevation = GlobalContainer.Player.transform.position.y - GlobalContainer.StartPosition.position.y;
        if (elevation > elevationRecord)
        {
            elevationRecord = elevation;
            // ������ ������������� ������ �� �(Consts.Player.ELEVATION_EXP_PERIOD) ������ ����� � �����(exp)
            if (elevationRecord - Consts.Player.ELEVATION_EXP_PERIOD > lastElevationRecord)
            {
                lastElevationRecord = elevationRecord;
                Exp++;
            }
                
        }

        GlobalContainer.ElevationText.text = $"Score: {elevation.ToString("F2")}" ;
    }

    public int Exp
    {
        get => exp;
        set
        {
            if (value >= 0)
                exp = value;

            GlobalContainer.ExpBar.value = exp;
            if (exp >= expBarMaxValue || exp >= 15)
                LevelUp();
        }
    }

    public uint Health
    {
        get => health;
        set
        {
            if (value <= Consts.Player.MAX_HEALTH)
            {
                // �� ������� �������� ���� ��� ������������ ����
                if (IsProtected && value < health)
                    return;

                health = value;
                GlobalContainer.HealthBar.value = health;
                if (health <= 0)
                    StartCoroutine(LoseEvent.GameoverTask(GlobalContainer));
            }          
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        ConfigExpBar();
        health = Consts.Player.MAX_HEALTH;
        currentLevel = 1;
    }

    // TODO ��������� ���� ��������
    IEnumerator LevelUpTask()
    {
        yield return new WaitForSeconds(2f);
        Exp++; 
        StartCoroutine(LevelUpTask());
    }

    void LevelUp()
    {
        CurrentLevel++;
        GlobalContainer.Player.CurrentMonster = GlobalContainer.MonsterSpawner.AllMonsters[CurrentLevel-1];
        GlobalContainer.SoundEffector.PlayLevelUp();
        MonsterNameText.text = ChangeName(GlobalContainer.Player.CurrentMonster.name);
        float newScale = Consts.Player.DEFAULT_MONSTER_SCALE * GetMonsterScaleFactor(GlobalContainer.Player.CurrentMonster.name);
        GlobalContainer.Player.CurrentMonster.transform.localScale = new Vector3(newScale, newScale, newScale);

        Exp = 0;
        expBarMaxValue = (CurrentLevel / 2);

        ConfigExpBar();
    }

    /// <summary>
    /// ������������� ����
    /// </summary>
    void ConfigExpBar()
    {
        GlobalContainer.ExpBar.maxValue = expBarMaxValue;
        GlobalContainer.ExpBar.value = 0;
    }

    public string ChangeName(string monsterName)
    {
        //string manipulation to get the name and id of the monster
        int idIndexStart = monsterName.IndexOf('_', 1);
        int nameIndexStart = monsterName.IndexOf('_', 9);
        int nameIndexEnd = monsterName.IndexOf('(', 9);


        return "Monster " + monsterName.Substring(idIndexStart + 1, nameIndexStart - idIndexStart - 1) + " : " + monsterName.Substring(nameIndexStart + 1, nameIndexEnd - nameIndexStart - 1);
    }

    /// <summary>
    /// ��� ������� ������� ���������� ��� scale
    /// </summary>
    /// <param name="monsterName"></param>
    /// <returns></returns>
    public static float GetMonsterScaleFactor(string monsterName)
    {
        float scalingFactor = 1;

        string[] bigMonsters = new string[] { "Rabbit", "Floating", "Yeti", "Wolf", "Mushroom", "Golem", "Shadow", "Skeleton", "Dragon", "Tree", "Crab", "Turtle", "Ghost", "Book", "Lizard", "Troll", "Masked" };
        string[] middleMonsters = new string[] { "Snail", "Mushroom", "Earth", "Moon", "Volcano", "Jupiter", "Planet", "Flower", "Cactus", "Block", "Mushroom", "Ox", "Zombie", "Ghoul", "Moumie", "Raptor", "Orc", "Snail", "Shell" };
        scalingFactor = NameContains(bigMonsters, monsterName) ? 0.55f : scalingFactor;
        scalingFactor = NameContains(middleMonsters, monsterName) ? 0.67f : scalingFactor;

        return scalingFactor;
    }

    /// <summary>
    /// �������� �� ��� ���� ���� ��������� �� ������� �������� 
    /// </summary>
    /// <param name="bigMonsters"></param>
    /// <param name="monsterName"></param>
    /// <returns></returns>
    public static bool NameContains(string[] bigMonsters, string monsterName)
    {
        foreach (var monsterChar in bigMonsters)
            if (monsterName.Contains(monsterChar))
                return true;

        return false;
    }

    public void ReloadLevel()
    {
        GlobalContainer.GoogleAds.AdBlockShow();
    }
}
