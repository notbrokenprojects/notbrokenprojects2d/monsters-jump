using Assets.Scripts;
using UnityEngine;

public class DestroyCheck : RatelWrapper
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // ������ ���������� ����� ������
        if (collision.tag == Consts.Player.PLAYER_TAG)
        {
            GlobalContainer.LevelController.Exp++;
            Destroy(gameObject.transform.parent.gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        // ������ ���������� ����� ������
        if (collision.gameObject.tag == Consts.Player.PLAYER_TAG)
        {
            GlobalContainer.LevelController.Exp++;
            Destroy(gameObject.transform.parent.gameObject); 
        }
    }
}
