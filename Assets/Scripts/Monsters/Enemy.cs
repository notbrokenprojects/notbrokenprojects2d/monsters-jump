using Assets.Scripts;
using Spine.Unity;
using System.Collections;
using System.Linq;
using UnityEngine;

public class Enemy : RatelWrapper
{
    bool isLeft;
    GameObject currentMonster;
    Transform attackCheck;
    SkeletonAnimation monsterAnimator; //The animator script of the monster
    float chance = Consts.Enemies.ENEMY_RESPAWN_CHANCE;

    public float Chance { get => chance; set => chance = value; }

    public GameObject CurrentMonster
    {
        get => currentMonster;
        set
        {

            // �������� ��������� �������
            var monsterPosition = currentMonster.transform.position;
            var monsterScale = currentMonster.transform.localScale;
            var oldMonster = currentMonster;

            currentMonster = Instantiate(value, transform);
            currentMonster.transform.position = monsterPosition;
            currentMonster.transform.localScale = monsterScale;

            monsterAnimator = currentMonster.GetComponent<SkeletonAnimation>();

            // ��������� ������� �������
            Destroy(oldMonster);
        }
    }   

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Patrol());
        StartCoroutine(PatrolAttack());
        currentMonster = transform.GetChild(transform.childCount - 1).gameObject;
        monsterAnimator = currentMonster.GetComponent<SkeletonAnimation>();

        attackCheck = transform.Find(Consts.Enemies.ENEMY_ATTACK_CHECK_TAG);
        GenerateRandomMonster();
        ChangeAnimation(Consts.AnimationStates.IDLE_STATE);
    }

    /// <summary>
    /// ������������� ���������� �������
    /// </summary>
    void GenerateRandomMonster()
    {
        int curIndex = Random.Range(0, 101) + 1;
        CurrentMonster = GlobalContainer.MonsterSpawner.AllMonsters[curIndex];
        float newScale = Consts.Player.DEFAULT_MONSTER_SCALE * LevelController.GetMonsterScaleFactor(CurrentMonster.name);
        CurrentMonster.transform.localScale = new Vector3(newScale, newScale, newScale);
    }

    IEnumerator Patrol()
    {
        yield return new WaitForSeconds(Consts.Enemies.ENEMY_PATROL_FLIP_DELAY);
        Flip();
        StartCoroutine(Patrol());
    }

    IEnumerator PatrolAttack()
    {
        yield return new WaitForSeconds(Consts.Enemies.ENEMY_PATROL_ATTACK_DELAY);

        ChangeAnimation(Consts.AnimationStates.ATTACK_STATE);
        if (IsHitToPlayer())
        {
            GlobalContainer.LevelController.Health--;
            GlobalContainer.Panel_damageEffect.SetActive(true);
        }         

        yield return new WaitForSeconds(Consts.Enemies.ENEMY_ANIMATION_ATTACK_DELAY);
        GlobalContainer.Panel_damageEffect.SetActive(false);
        ChangeAnimation(Consts.AnimationStates.IDLE_STATE);

        StartCoroutine(PatrolAttack());
    }

    /// <summary>
    /// ��������� ��������� �� ������
    /// </summary>
    /// <returns></returns>
    bool IsHitToPlayer()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(attackCheck.position, 0.5f);
        var playerColliders = colliders.Where(c => c.gameObject.tag == Consts.Player.PLAYER_TAG).ToArray();
        return playerColliders.Count() > 0;
    }

    void Flip()
    {
        if (isLeft)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            isLeft = false;
        }
        else
        {
            transform.localRotation = Quaternion.Euler(0, 180, 0);
            isLeft = true;
        }

    }

    /// <summary>
    /// �������� ��������
    /// </summary>
    /// <param name="AnimationName"></param>
    public void ChangeAnimation(string AnimationName)  //Names are: Idle, Walk, Dead and Attack
    {
        if (monsterAnimator == null)
            return;

        bool IsLoop = true;

        //set the animation state to the selected one
        monsterAnimator.AnimationState.SetAnimation(0, AnimationName, IsLoop);
    }
}
